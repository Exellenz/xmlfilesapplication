﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using XmlFilesManager.Models;

namespace XmlFilesManager.DAL
{
    public class XmlFilesInitializer : DropCreateDatabaseIfModelChanges<XmlFilesContext>
    {
        protected override void Seed(XmlFilesContext context)
        {
            var files = new List<File>
            {
                new File { Name = "тестовыйфайл_3_file.xml", Version = "1.0", LastChange = DateTime.Now },
                new File { Name = "файл_0123456789_lll.xml", Version = "1.0", LastChange = DateTime.Now },
                new File { Name = "файл_12345678901234_2.xml", Version = "1.0", LastChange = DateTime.Now },
                new File { Name = "хмлфайл_0_abcabca.xml", Version = "1.0", LastChange = DateTime.Now },
                new File { Name = "файл_2_11.xml", Version = "1.0", LastChange = DateTime.Now },
            };
            files.ForEach(f => context.Files.Add(f));

            var logs = new List<Log>
            {
                new Log { FileId = 5, ChangeTime = DateTime.Now, Action = "Файл загружен" },
                new Log { FileId = 1, ChangeTime = DateTime.Now, Action = "Файл загружен" },
                new Log { FileId = 2, ChangeTime = DateTime.Now, Action = "Файл загружен" },
                new Log { FileId = 3, ChangeTime = DateTime.Now, Action = "Файл загружен" },
                new Log { FileId = 4, ChangeTime = DateTime.Now, Action = "Файл загружен" }
            };
            logs.ForEach(l => context.Logs.Add(l));

            context.SaveChanges();
        }
    }
}