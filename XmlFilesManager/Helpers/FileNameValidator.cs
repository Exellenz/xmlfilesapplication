﻿using System.IO;
using System.Text.RegularExpressions;

namespace XmlFilesManager.Helpers
{
    public class FileNameValidator
    {
        private static string _pattern = @"^[А-Яа-я]{0,100}_(\d|\d{10}|\d{14,20})_[\w\-. ]{0,7}\.xml$";

        public static bool IsValid(string filename)
        {
            return filename != null && new Regex(_pattern).IsMatch(GetNameFromPath(filename));
        }

        public static string GetNameFromPath(string path)
        {
            return Path.GetFileName(path);
        }
    }
}