﻿namespace XmlFilesManager.Helpers
{
    public class Difference
    {
        public string FieldName;
        public string OldValue;
        public string NewValue;

        public Difference(string fieldName, string oldValue, string newValue)
        {
            FieldName = fieldName;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}