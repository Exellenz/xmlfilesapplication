﻿using System;
using System.Web;
using System.Xml;
using XmlFilesManager.Models;

namespace XmlFilesManager.Helpers
{
    public class XmlFileParser
    {
        public static File Parse(HttpPostedFileBase postedFile)
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(postedFile.InputStream);

            var resultFile = new File();

            if (xmlDocument.ChildNodes.Count != 1) return resultFile;

            var fileNode = xmlDocument.ChildNodes[0];
            if (fileNode.ChildNodes.Count == 2 && fileNode.Attributes?["FileVersion"] != null)
            {
                resultFile.Version = fileNode.Attributes["FileVersion"].Value;
                var firstNode = fileNode.ChildNodes[0];
                var secondNode = fileNode.ChildNodes[1];

                if (firstNode.Name.Equals("Name"))
                {
                    var name = firstNode.ChildNodes[0]?.Value;
                    resultFile.Name = FileNameValidator.IsValid(name) ? name : null;
                }
                
                if (secondNode.Name.Equals("DateTime"))
                {
                    try
                    {
                        resultFile.LastChange = DateTime.Parse(secondNode.ChildNodes[0]?.Value);
                    }
                    catch (FormatException) {}
                }
            }

            return resultFile;
        }

        public static XmlDocument GetXml(File file)
        {
            var resultXml = new XmlDocument();

            var fileNode = resultXml.CreateElement("File");
            var fileAttribute = resultXml.CreateAttribute("FileVersion");
            fileAttribute.Value = file.Version;
            fileNode.Attributes.Append(fileAttribute);
            resultXml.AppendChild(fileNode);

            var nameNode = resultXml.CreateElement("Name");
            nameNode.AppendChild(resultXml.CreateTextNode(file.Name));
            fileNode.AppendChild(nameNode);

            var dateTimeNode = resultXml.CreateElement("DateTime");
            dateTimeNode.AppendChild(resultXml.CreateTextNode(file.LastChange.ToString(@"dd\.MM\.yyyy HH:mm:ss")));
            fileNode.AppendChild(dateTimeNode);

            return resultXml;
        }
    }
}