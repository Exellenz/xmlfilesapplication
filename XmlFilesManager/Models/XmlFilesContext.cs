﻿using System.Data.Entity;

namespace XmlFilesManager.Models
{
    public class XmlFilesContext : DbContext
    {
        public DbSet<File> Files { get; set; }
        public DbSet<Log> Logs { get; set; }
    }
}