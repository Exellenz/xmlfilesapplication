﻿using System;
using System.ComponentModel.DataAnnotations;

namespace XmlFilesManager.Models
{
    public class Log
    {
        [Key]
        public int Id { get; set; }
        public int FileId { get; set; }
        public DateTime ChangeTime { get; set; }
        public string Action { get; set; }
    }
}