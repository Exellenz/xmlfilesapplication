﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using XmlFilesManager.Helpers;

namespace XmlFilesManager.Models
{
    public class File : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        public string Version { get; set; }
        public string Name { get; set; }
        public DateTime LastChange { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!FileNameValidator.IsValid(Name))
            {
                yield return new ValidationResult("Неверный формат имени файла");
            }
        }
    }
}