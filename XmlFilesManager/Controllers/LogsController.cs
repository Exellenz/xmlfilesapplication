﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using XmlFilesManager.Helpers;
using XmlFilesManager.Models;

namespace XmlFilesManager.Controllers
{
    public class LogsController : Controller
    {
        private readonly XmlFilesContext _db = new XmlFilesContext();

        [HttpGet]
        public ActionResult Index(int? fileId)
        {
            if (fileId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var file = _db.Files.Find(fileId);
            ViewBag.FileName = file?.Name;
            ViewBag.FileId = fileId;

            var logs = _db.Logs.Where(l => l.FileId == fileId);
            return View(logs.ToList());
        }

        public void AddCreationLog(int fileId)
        {
            var log = new Log
            {
                FileId = fileId,
                Action = GetCreationAction(),
                ChangeTime = DateTime.Now
            };
            _db.Logs.Add(log);
            _db.SaveChanges();
        }

        public void AddEditionLog(IEnumerable<Difference> differences, int fileId)
        {
            foreach (var difference in differences)
            {
                var log = new Log
                {
                    FileId = fileId,
                    Action = GetEditionAction(difference),
                    ChangeTime = DateTime.Now
                };
                _db.Logs.Add(log);
            }
            _db.SaveChanges();
        }

        public void AddDownloadingLog(int fileId)
        {
            var log = new Log
            {
                FileId = fileId,
                Action = GetDownloadingAction(),
                ChangeTime = DateTime.Now
            };
            _db.Logs.Add(log);
            _db.SaveChanges();
        }

        private string GetCreationAction()
        {
            return "Файл сохранен в базу";
        }

        private string GetEditionAction(Difference difference)
        {
            return
                $"'{difference.FieldName}' field changed: old value - '{difference.OldValue}', new value - '{difference.NewValue}'";
        }

        private string GetDownloadingAction()
        {
            return "Файл был скачан";
        }
    }
}