﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using XmlFilesManager.Helpers;
using XmlFilesManager.Models;

namespace XmlFilesManager.Controllers
{
    public class FilesController : Controller
    {
        private readonly XmlFilesContext _db = new XmlFilesContext();

        [HttpGet]
        public ActionResult Index()
        {
            return View(_db.Files.ToList());
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return View("Index");
            }
            var file = _db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            var fileName = upload?.FileName;
            if (!FileNameValidator.IsValid(fileName))
            {
                return View();
            }

            var parsedFile = XmlFileParser.Parse(upload);

            if (parsedFile != null)
            {
                _db.Files.Add(parsedFile);
                _db.SaveChanges();

                new LogsController().AddCreationLog(parsedFile.Id);

                return RedirectToAction("Details", "Files", new {id = parsedFile.Id});
            }

            return View();
        }

        [HttpGet]
        public ActionResult Download(int? id)
        {
            var file = _db.Files.Find(id);

            if (file == null)
            {
                return null;
            }

            var xmlFile = XmlFileParser.GetXml(file);

            new LogsController().AddDownloadingLog(file.Id);

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename="+ HttpUtility.UrlEncode(file.Name, Encoding.UTF8));
            Response.ContentType = "text/csv";
            xmlFile.Save(Response.OutputStream);
            Response.End();
            
            return null;
        }

        [HttpPost]
        public ActionResult GetPreview()
        {
            if (Request.Files.Count != 1)
            {
                return View("Error", new Error {Message = "Неверное количество файлов"});
            }

            var postFile = Request.Files["uploadedFile"];

            if (postFile == null)
            {
                return null;
            }

            if (!FileNameValidator.IsValid(postFile.FileName))
            {
                return View("Error", new Error { Message = "Неверный формат имени файла" });
            }

            var parsedFile = XmlFileParser.Parse(postFile);
            return 
                parsedFile == null 
                ? View("Error", new Error { Message = "Неправильная структура файла" })
                : View("Preview", parsedFile);
        }
        
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return View("Index");
            }
            var file = _db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Version,Name,LastChange")] File file)
        {
            if (ModelState.IsValid)
            {
                var oldFile = _db.Files.AsNoTracking().First(f => f.Id == file.Id);
                var differences = GetDifferences(oldFile, file);
                new LogsController().AddEditionLog(differences, file.Id);

                file.LastChange = DateTime.Now;
                _db.Entry(file).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(file);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private IEnumerable<Difference> GetDifferences(File oldFile, File newFile)
        {
            var differences = new List<Difference>();

            if (!oldFile.Name.Equals(newFile.Name))
            {
                differences.Add(new Difference("Name", oldFile.Name, newFile.Name));
            }
            if (!oldFile.Version.Equals(newFile.Version))
            {
                differences.Add(new Difference("Version", oldFile.Version, newFile.Version));
            }

            return differences;
        }
    }
}
