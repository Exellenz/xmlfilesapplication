﻿using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using XmlFilesManager.DAL;

namespace XmlFilesManager
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Database.SetInitializer(new XmlFilesInitializer());
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
